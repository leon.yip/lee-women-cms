var curPage = 1;

//get date and time
var date = new Date();
var days = 365;
date.setTime(date.getTime() + (days*24*60*60*1000));

var page = {
	index:function(){
		$('.page').hide();
		$('#index').show();
	},
	login:function(){
		$('#index').hide();
		$('#content').show();
		$('#header').show();
		$('#main').show();
		$('#p1').show();
	},
	change:function(page){
		window.location.hash = '#' + page;
		$('.overlay').hide();
		$('.page').hide();
		$('#p'+page).show();
	},
	hide:function(page){
		$('#p'+page).hide();
	}
};

$('.menu li').click(function(){
	if ($(this).hasClass('disable')) {
		return;
	}else{
		$('.menu li').removeClass('active');
		$(this).addClass('active');
		var ch = $(this).attr('page');
		page.change(ch);
		curPage = ch;
	}
});

$('.nPageBtn').click(function(){
	curPage++;
	if (showPrizeComponent===false) {
		if (curPage===11||curPage===12) {
			curPage = 13;
		}
	}
	$('.menu li').removeClass('active');
	$('#p'+curPage+'Btn').addClass('active');
		page.change(curPage);
});

$(window).on('hashchange', function() {
	hash = window.location.hash.substring(1);
	if (hash==='') {
		page.index();
	}
	else{
		page.change(hash);
	}
});

function getParam(name)
{
  name = name.replace(/[\[]/,'\\\[').replace(/[\]]/,'\\\]');
  var regexS = '[\\?&]'+name+'=([^&#]*)';
  var regex = new RegExp( regexS );
  var results = regex.exec( window.location.href );
  if( results === null ){
    return;
	}
  else{
		return results[1];
	}
}
