var countryCode;
var previewCopies;
var previewAssets;
var previewQuotes;
var previewResults;
var previewPrize;
var published='0';
var previewLang;

function getData(lang,preview){
	var data;
	var api;

	data={
		'lang':countryCode
	};

	// if (!preview) {
	// 		data={
	// 			'lang':lang
	// 		};
	// 		api = 'onPreview';
	// }
	// else{
	// 	data={
	// 		'lang':lang,
	// 		'preview':preview
	// 	};
	// 	api = 'getJSON';
	// }

	$.post( serverURL + 'api/onPreview',data, function(result)
	{
		if (result==='no record') {
			alert('no record!');
		}

		date = result.date;
		published = result.published;
		previewCopies = result.copies;
		previewResults = result.results;
		previewAssets = result.images;
		previewQuotes = result.quotes;
		previewPrize = result.prize;
		previewLang = result.languages;

		//Date
		$('.latesUpdated').html(new Date(result.date));

		//Country code
		$('.countryCode').html(countryCode);

		//Published
		if (!published) {
			published='0';
		}
		if (published==='0') {
			$('.status').html('Preview');
		}else{
			$('.status').html('Published');
		}

		//lang selector
		console.log(previewLang);
		if (!previewLang) {
			$('.selectLang').hide();
		}

		//copy loop
		$.each(previewCopies,function(key,object){

			if (key==='section.tc .copy span') {
				$('#editor_TC .ql-editor').html(object);
			}
			else if (key==='section.shops .copy span') {
				$('#editor_SL .ql-editor').html(object);
			}
			else if (key==='div.popupDianping .copy span') {
				$('#editor_RDO .ql-editor').html(object);
			}
			else if(key==='section.loading .copy span'){

				var loadingArr = object;
				$.each(loadingArr,function(index,data){
					document.getElementById('loading_'+index).value = data;
				});
			}
			else{
				if(!document.getElementById(key))
					return;
				document.getElementById(key).value = object;
			}
		});

		//copies
		$.each(previewAssets,function(key,object){
			if(!document.getElementById(key))
				return;
			document.getElementById(key).setAttribute('src',object);
		});

		//result
		$.each(previewResults,function(key,object){
			if (key==='q1Labels'|| key==='q2Labels'|| key==='q3Labels') {
				$.each(previewResults[key],function(index,obj){
					document.getElementById(index).value = obj;
				});
			}
			else if (previewResults[key]==='prize') {
				previewPrize = object;
			}
		});

		//images
		$.each(previewQuotes,function(key,object){
			if(!document.getElementById(key))
				return;
			document.getElementById(key).setAttribute('src',object);
		});

    var group = 'input:checkbox';
    $(group).prop('checked', false);
		document.getElementById('prize_'+previewPrize).checked = true;
		curPrize = previewPrize;
		prizeComponent();

		document.getElementById('tokyoArt').value = previewResults['types']['tokyo']['art'];
		document.getElementById('tokyoFeminine').value = previewResults['types']['tokyo']['style']['feminine'];
		document.getElementById('tokyoClassic').value = previewResults['types']['tokyo']['style']['classic'];
		document.getElementById('tokyoBoyish').value = previewResults['types']['tokyo']['style']['boyish'];
		document.getElementById('tokyoPlayful').value = previewResults['types']['tokyo']['style']['playful'];
		document.getElementById('parisArt').value =previewResults['types']['paris']['art'];
		document.getElementById('parisFeminine').value = previewResults['types']['paris']['style']['feminine'];
		document.getElementById('parisClassic').value =previewResults['types']['paris']['style']['classic'];
		document.getElementById('parisBoyish').value =previewResults['types']['paris']['style']['boyish'];
		document.getElementById('parisPlayful').value=previewResults['types']['paris']['style']['playful'];
		document.getElementById('nycArt').value=previewResults['types']['nyc']['art'];
		document.getElementById('nycFeminine').value=previewResults['types']['nyc']['style']['feminine'];
		document.getElementById('nycClassic').value =previewResults['types']['nyc']['style']['classic'];
		document.getElementById('nycBoyish').value =previewResults['types']['nyc']['style']['boyish'];
		document.getElementById('nycPlayful').value=previewResults['types']['nyc']['style']['playful'];
		document.getElementById('amsterdamArt').value=previewResults['types']['amsterdam']['art'];
		document.getElementById('amsterdamFeminie').value=previewResults['types']['amsterdam']['style']['feminine'];
		document.getElementById('amsterdamClassic').value=previewResults['types']['amsterdam']['style']['classic'];
		document.getElementById('amsterdamBoyish').value=previewResults['types']['amsterdam']['style']['boyish'];
		document.getElementById('amsterdamPlayful').value=previewResults['types']['amsterdam']['style']['playful'];
		document.getElementById('londonArt').value=previewResults['types']['london']['art'];
		document.getElementById('londonFeminine').value=previewResults['types']['london']['style']['feminine'];
		document.getElementById('londonClassic').value=previewResults['types']['london']['style']['classic'];
		document.getElementById('londonBoyish').value=previewResults['types']['london']['style']['boyish'];
		document.getElementById('londonPlayful').value=previewResults['types']['london']['style']['playful'];
		document.getElementById('berlinArt').value=previewResults['types']['berlin']['art'];
		document.getElementById('berlinFeminine').value=previewResults['types']['berlin']['style']['feminine'];
		document.getElementById('berlinClassic').value=previewResults['types']['berlin']['style']['classic'];
		document.getElementById('berlinBoyish').value=previewResults['types']['berlin']['style']['boyish'];
		document.getElementById('berlinPlayful').value=previewResults['types']['berlin']['style']['playful'];
	});
}
