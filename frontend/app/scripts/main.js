var showPrizeComponent = true;
var curCopies={};
var curAssets={};
var curResults={};
var curQuotes={};
var curPrize = 'on';

function overlay(e){
	$('#'+e).show();
}

function getReady() {
    // console.log('start preloading');
		page.index();
    var imageNames = [
			'images/bg.jpg'
    ];
    var imagesCount = imageNames.length;
    var resourceCount = imagesCount;
    var loadedResourceCount = 0;
    var images = [];
    for (var i = 0; i < imagesCount; i++) {
        images[i] = new Image();
        images[i].src = imageNames[i];
        images[i].onload = function () {
            var progress = Math.ceil(100 * (++loadedResourceCount / resourceCount));
            console.log(progress);
            // $('#progress').text(progress + '%');
            if (loadedResourceCount >= resourceCount) {

							// page.login();
            }
        }
    }
}

function readURL(input) {
		var id = input.id;
		var inputBtn = input;

    if (inputBtn.files && inputBtn.files[0]) {
      var reader = new FileReader();
      reader.readAsDataURL(inputBtn.files[0]);

      reader.onload = function (e) {
				var image = new Image();
				image.src = reader.result;
				 image.onload = function() {
          var w = this.width,
              h = this.height,
              t = inputBtn.files[0].type,
              n = inputBtn.files[0].name,
              s = (inputBtn.files[0].size/1024);

							if (s>500) {
								alert('img too large');
								return;
							}
							else{
								var fileType = inputBtn.getAttribute('fileType');
								var imageData = {
									'imageData': e.target.result,
									'fileType':fileType
								};

								$.post(serverURL+'api/imgUpload',imageData)
									.done(function(res){
										console.log(res);
										// $(inputBtn).parent().find('.uploaded span').html('Upload Success!');
										$(inputBtn).parent().find('.preview').attr('src', res);
								});
							}
        };
      };
    }
}

$('.fileInput').change(function(){
  readURL(this);
});

$(document).on('click','.fileBtn',function(){
	$(this).parent().find('.fileInput').click();
});

$(document).on('change','.quoteCon .uploadCon .fileInput',function(){
  readURL(this);
});

$(document).on('change','input',function(){
  launchReady = '0';
});

function getquote(){
		$('.quoteCon').html('');

	for (var i = 1; i < 25; i++) {
		var img = '<div class="uploadCon"><img id="quote_'+i+'" class="preview" src="images/120x120.jpg" alt=""/><input id="quote_'+i+'" type="file" class="fileInput" multiple fileType="quotes"><div class="uploaded"><span></span></div><div id="" class="fileBtn btn" type="button"><span>Quote '+i+'</span></div><div class="uploadBtn btn"><span>Upload</span></div></div>';

		$('.quoteCon').append(img);
	}
}

$('.logoutBtn').on('click', function() {
	location.reload();
});

$('input:checkbox').on('click', function() {
  var $box = $(this);
	console.log($box);
  if ($box.is(':checked')) {
    var group = 'input:checkbox[name="' + $box.attr('name') + '"]';
    $(group).prop('checked', false);
    $box.prop('checked', true);
  } else {
    $box.prop('checked', false);
  }

	curPrize = $box.attr('prize');

	prizeComponent();
});

function prizeComponent(){
	if (curPrize==='on') {
		$('.prizeStaff').addClass('disable');
		$('.prizeCode').removeClass('disable');
		$('.prizeOff').removeClass('disable');
		$('.prizeComponent').show();
		$('.nocode').show();
		$('.nocodeImg').hide();
		$('.dianpingImg').show();
		$('#prizeTitle').html('Prize with unique code');
	}
	else if(curPrize==='nocode'){
		$('.prizeStaff').addClass('disable');
		$('.prizeCode').removeClass('disable');
		$('.prizeOff').removeClass('disable');
		$('.prizeComponent').show();
		$('.nocodeImg').show();
		$('.nocode').hide();
		$('.dianpingImg').hide();
		$('#prizeTitle').html('Prize without code');
	}
	else if(curPrize==='staff'){
		$('.prizeStaff').removeClass('disable');
		$('.prizeCode').addClass('disable');
		$('.prizeOff').removeClass('disable');
		$('.prizeComponent').show();
		$('.nocodeImg').hide();
		$('.dianpingImg').show();
	}
	else if(curPrize==='off'){
		$('.prizeStaff').addClass('disable');
		$('.prizeCode').addClass('disable');
		$('.prizeOff').addClass('disable');
		$('.prizeComponent').hide();
	}
}

function save(save){

	$.each(sampleCopies,function(key,object){

		if (key==='section.tc .copy span') {
			curCopies[key] = $('#editor_TC .ql-editor').html();
		}
		else if (key==='section.shops .copy span') {
			curCopies[key] = $('#editor_SL .ql-editor').html();
		}
		else if (key==='div.popupDianping .copy span') {
			curCopies[key] = $('#editor_RDO .ql-editor').html();
		}
		else if(key==='section.loading .copy span'){

			var loadingArr = [];
			loadingArr.push(document.getElementById('loading_0').value);
			loadingArr.push(document.getElementById('loading_1').value);
			loadingArr.push(document.getElementById('loading_2').value);
			loadingArr.push(document.getElementById('loading_3').value);
			loadingArr.push(document.getElementById('loading_4').value);
			loadingArr.push(document.getElementById('loading_5').value);
			loadingArr.push(document.getElementById('loading_6').value);
			curCopies[key] = loadingArr;
		}
		else{
			curCopies[key] = document.getElementById(key).value;
		}
	});

	$.each(sampleAssets,function(key,object){
		curAssets[key] = document.getElementById(key).getAttribute('src');
	});

	$.each(sampleQuotes,function(key,object){
		curQuotes[key] = document.getElementById(key).getAttribute('src');
	});

	$.each(sampleResults,function(key,object){
		curResults[key] = sampleResults[key];
		if (sampleResults[key]!=='types' || sampleResults[key]!=='prize') {
			$.each(sampleResults[key],function(index,object){
				curResults[key][index]=document.getElementById(index).value;
			});
		}
		else if (previewResults[key]==='prize') {
			curResults[key][index] = curPrize;
		}
	});

	curResults['types'] = {
		'tokyo': {
			'art' : document.getElementById('tokyoArt').value,
			'style':
			{
				'feminine'	: document.getElementById('tokyoFeminine').value,
				'classic'	: document.getElementById('tokyoClassic').value,
				'boyish'	: document.getElementById('tokyoBoyish').value,
				'playful'	: document.getElementById('tokyoPlayful').value
			}
		},

	'paris' : {
		'art'			: document.getElementById('parisArt').value,
		'style'			:
			{
				'feminine'	: document.getElementById('parisFeminine').value,
				'classic'	: document.getElementById('parisClassic').value,
				'boyish'	: document.getElementById('parisBoyish').value,
				'playful'	: document.getElementById('parisPlayful').value
			}
		},

		'nyc' : {
			'art'		: document.getElementById('nycArt').value,
			'style'		:
				{
					'feminine'	: document.getElementById('nycFeminine').value,
					'classic'	: document.getElementById('nycClassic').value,
					'boyish'	: document.getElementById('nycBoyish').value,
					'playful'	: document.getElementById('nycPlayful').value
				}
		},

		'amsterdam' : {
			'art'		: document.getElementById('amsterdamArt').value,
				'style'		:
				{
					'feminine'	: document.getElementById('amsterdamFeminie').value,
					'classic'	: document.getElementById('amsterdamClassic').value,
					'boyish'	: document.getElementById('amsterdamBoyish').value,
					'playful'	: document.getElementById('amsterdamPlayful').value
				}
		},

		'london' : {
			'art'		: document.getElementById('londonArt').value,
			'style'		:
				{
					'feminine'		: document.getElementById('londonFeminine').value,
					'classic'		: document.getElementById('londonClassic').value,
					'boyish'		: document.getElementById('londonBoyish').value,
					'playful'		: document.getElementById('londonPlayful').value
				}
		},

		'berlin' : {
			'art'		: document.getElementById('berlinArt').value,
			'style'		:
				{
					'feminine'	: document.getElementById('berlinFeminine').value,
					'classic'	: document.getElementById('berlinClassic').value,
					'boyish'	: document.getElementById('berlinBoyish').value,
					'playful'	: document.getElementById('berlinPlayful').value
				}
			}
	};

	var copiesObj = JSON.stringify(curCopies);
	var assetsObj = JSON.stringify(curAssets);
	var quotesObj = JSON.stringify(curQuotes);
	var resultsObj = JSON.stringify(curResults);

	var data = {
		'lang':countryCode,
		'published':save,
		'copies':copiesObj,
		'results':resultsObj,
		'images':assetsObj,
		'quotes':quotesObj,
		'prize':curPrize,
		'languages':previewLang
	};

	$.post(serverURL+'api/save',data)
		.done(function(res){
			console.log(res);
			if (res==='update') {
				launchReady='1';

				var ele = 'images/qrcode/'+countryCode+'.png';
				var w = 200;
				var left = (screen.width/2)-(w/2);
			  var top = (screen.height/2)-(w/2);
				console.log(ele);
				// $('#qrcode').attr('src',ele);

				$('#qrcode').html('');
		    new QRCode(document.getElementById('qrcode'), 'http://www.leewomen.com/?lang='+countryCode+'&published=0');
				$('#qrcodeOver').show();

				// var spec ='location=0,menubar=0,status=0,toolbar=0,height=200,width=200,left='+left+',top='+top;
					// window.open(ele,'Lee Women',spec);
			}
			else{
				window.scrollTo(0,0);
				launchReady='0';
			}
			getData(countryCode);
	});
}

function confirmPublish()
{
	if (launchReady==='0') {
		alert('Please save and preview before publish');
	}else{
	  var r = confirm('Confirm Publish?');
	  if (r === true) {
	    save('1');
			launchReady='0';
	  }
	}
}

$('#couponBtn').click(function() {
	var fileVal=document.getElementById('fileinput');
  var file = fileVal.files[0];

	if (file===undefined) {
		alert('Please select file');
		return;
	}

	var r = confirm('Overwirte coupons?');
	if (r === true) {
		var reader = new FileReader();
		reader.onload = function(progressEvent){
			var country = countryCode.split('_');

			// By lines
			var dataArr=[];
			var data={
				'country':country[0],
				'dataArr':dataArr
			};
			var lines = this.result.split('\n');
			for(var line = 0; line < lines.length; line++){
				if(!lines[line]){
					continue;
				}

				dataArr.push(lines[line]);
			}
			console.log(data);

			$.post(serverURL+'api/coupon_upload',data)
				.done(function(res){
					if (res==='done') {
						alert('Upload Complete');
					}else{
						alert('Connection Error!');
					}
				});
		};
		reader.readAsText(file);
	}
});
