var sampleCopies ={
		// "title"						: "Lee Women",
		"section.loading .label span" 		: "Loading...",
		// menu
		"section.menu .playnow" 	: "PLAY NOW",
		"section.menu .gallery" 	: "GALLERY",
		"section.menu .reward" 		: "ENTER GRAND DRAW",
		"section.menu .shops" 		: "STORE LIST",
		"section.menu .tc" 			: "TERMS & CONDITIONS",
		// intro
		".intro1 .copy span" 				: "Every stroke mimics each step we take. Some results expected, many surprising. But no matter the outcome, you can always paint them over.",
		".intro1 .cta span" 				: "WHAT WILL<br>YOUR CANVAS BE?",
		".intro3 .title3 span" 				: "Upload or take your photo",
		// edit
		"section.edit .title span" 		: "EDIT YOUR PHOTO",
		"section.edit .pinch" 			: "Pinch screen to zoom in or out",
		// "section.edit .reload span" 	: "",
		"section.edit .nextButton span" : "NEXT",
		// q1
		"section.q1 .title span" 		: "WE BELIEVE PERSONAL STYLE COMES FROM WITHIN, AND EVOLVE WITH YOU DAY BY DAY AND BIT BY BIT",
		"section.q1 .lee span" 			: "- Lee women",
		"section.q1 .nextButton span"	: "NEXT",
		"section.q1 .question span"		: "WHAT STYLE DO YOU WANT TO TRY NEXT?",
		// q2
		"section.q2 .title span" 		: "WE INSPIRE OUR FRIENDS WITH NEW THINGS TO DO AND OFFBEAT PLACES TO SEE, CREATING MOMENTS WE CAN LOOK BACK TO SAY \'REMEMBER WHEN...\'",
		"section.q2 .lee span" 			: "- Lee women",
		"section.q2 .nextButton span"	: "NEXT",
		"section.q2 .question span"		: "WHAT NEW MEMORIES DO YOU WANT TO CREATE WITH YOUR FREINDS NEXT?",
		// q3
		"section.q3 .title span" 		: "OUR CURIOSITY TAKES US TO CITIES WE\'VE NEVER BEEN TO AND PEOPLE WE\'VE NEVER MET",
		"section.q3 .lee span" 			: "- Lee women",
		"section.q3 .question span"		: "WHERE WILL YOUR CURIOSITY TAKE YOU NEXT?",
		"section.q3 .nextButton span"	: "NEXT",
		// swipe
		"div.swipeTip div"			: "SWIPE LEFT OR RIGHT TO SELECT, PRESS \'NEXT \' TO PROCEED",
		// PREVIEW
		"section.preview .title span" 		: "EXPLORE YOUR CANVAS",
		"section.preview .continue span"	: "CONTINUE YOUR EVOLUTION?",
		"section.preview .instruction span" : "Shake for a new canvas<br>or press OK to proceed",
		"section.preview .nextButton span" 		: "OK",
		// PREVIEW CONFIRM
		"div.submitConfirm .share span" 		: "SHARE YOUR CANVAS",
		"div.submitConfirm .option span" 		: "Agree to display in thegallery?",
		"div.submitConfirm .button span" 		: "SUBMIT",
		// result
		"section.result .redeem span" 		: "REDEEM PRIZE",
		".popupDownload .copy span"			: "TAP AND HOILD<br> TO DOWNLOAD TO YOUR CAMERA ROLL",
		// redeeem
		'section.redeem .title span' 		: 'CONGRATULATIONS!',
		'section.redeem .copy span' 		: 'You have won xxxxx',
		'section.redeem .details span' 		: 'Redemption details',
		'section.redeem .dianping span'		: 'REDEEM NOW',
		'section.redeem .draw span' 		: 'ENTER GRAND DRAW',
		'section.redeem .gallery span' 		: 'VISIT GALLERY',

		// popup redeem
		'div.popupDianping .title span'		: 'How can I redeem the prize!',
		'div.popupDianping .copy span'		: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit',
		// 'div.popupDianping .details span'	: '<ul><li>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore!</li><li>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore!</li></ul>',

		//prize overlay
		'section.redeemDianping .title span' : 'Prize details',
		'section.redeemDianping .instruction span': 'Instructions',
		'section.redeemDianping .copy span'	: '1. First do this and this and that<br>2. Then, do this and this',

		//tc
		'section.tc .title span' 		: 'Lee women<br>Lorem ipsum dolor!',
		'section.tc .copy span' 		: '<b>Larem Ipsum:</b><ol><li>consectetur adipiscing elit, sed do eiusmod tempor incididunt</li><li>consectetur adipiscing elit, sed do eiusmod tempor incididunt</li><li>consectetur adipiscing elit, sed do eiusmod tempor incididunt</li><li>consectetur adipiscing elit, sed do eiusmod tempor incididunt</li><li>consectetur adipiscing elit, sed do eiusmod tempor incididunt</li><li>Time: <br>- 2015/2/2/ - 2015/2/2</li><li>Dsddsd:<ol><li>consectetur adipiscing elit, sed do eiusmod tempor incididunt</li><li>consectetur adipiscing elit, sed do eiusmod tempor incididunt</li><li>consectetur adipiscing elit, sed do eiusmod tempor incididunt</li></ol></li><li>Lorem:<br>consectetur adipiscing elit, sed do eiusmod tempor incididunt<br>consectetur adipiscing elit, sed do eiusmod tempor incididunt</li><li>consectetur adipiscing elit, sed do eiusmod tempor incididuntconsectetur adipiscing elit, sed do eiusmod tempor incididunt</li><li>参加者须保证上传照片由他/consectetur adipiscing elit, sed do eiusmod tempor incididuntconsectetur adipiscing elit, sed do eiusmod tempor incididunt</li><li>consectetur adipiscing elit, sed do eiusmod tempor incididunt<ol><li>consectetur adipiscing elit, sed do eiusmod tempor incididunt IP consectetur adipiscing elit, sed do eiusmod tempor incididunt</li><li>consectetur adipiscing elit, sed do eiusmod tempor incididunt</li><li>consectetur adipiscing elit, sed do eiusmod tempor incididunt</li></ol>consectetur adipiscing elit, sed do eiusmod tempor incididunt</li><li>consectetur adipiscing elit, sed do eiusmod tempor incididunt consectetur adipiscing elit, sed do eiusmod tempor incididuntconsectetur adipiscing elit, sed do eiusmod tempor incididunt</li><li>consectetur adipiscing elit, sed do eiusmod tempor incididunt</li><li>consectetur adipiscing elit, sed do eiusmod tempor incididuntconsectetur adipiscing elit, sed do eiusmod tempor incididunt</li></ol>',

		//draw
		'section.draw .title span' : 'Get a cool lee women prize',
		'section.draw .fill span': 'Please fill in the following information to enter:',
		'section.draw .remark span'	: '*MANDATORY FIELDS',
		'INPUT_NAME'					: 'Full name*',
		'INPUT_EMAIL'					: 'Email address*',
		'INPUT_PHONE'					: 'Phone number',
		'section.draw .button span'		: 'SUBMIT',
		// popupDraw
		'div.popupDraw .copy span'				: 'Are all your details correct?',
		'div.popupDraw .nameLabel span.label'	: 'Full name: ',
		'div.popupDraw .emailLabel span.label' 	: 'Email address: ',
		'div.popupDraw .numberLabel span.label' : 'Phone number: ',
		'div.popupDraw .button span' 			: 'Confirm',
		// thankyou
		'section.thankyou .title span'			: 'THNAKS FOR PARTICIPATING</br>AND GOOD LUCK!',
		'section.thankyou .copy span'			: 'Winners will be notified via email by<br> xxxxxx (tbc)',
		'section.thankyou .gallery span'		: 'Gallery',
		'section.thankyou .again span'			: 'Play again',
		// gallery
		'section.gallery .subtitle span'		: 'GALLERY',
		'section.gallery .button span'			: 'MORE CANVAS',
		'div.galleryMenu div'					: 'GALLERY LIST',
		//loading
		'section.loading .copy span'	: [
			"She notices details nobody sees, and appreciates the art of living",
			"She constantly tests the rules and boundaries of society",
			"She’s constantly learning how she is through navigating the world",
			"The world around her is changing so rapidly, her greatest fear is falling behind In order to keep up, she’s in a constant state of evolving and refining.",
			"She doesn’t try to be glamorous or superior She’s a real woman, not a supermodel",
			"She doesn’t try to be glamorous or superior She’s a real woman, not a supermodel",
			"She doesn’t try to be glamorous or superior She’s a real woman, not a supermodel"
		],

		// staff
		'section.redeemStaff .title span'		: 'HOW CAN I REDEEM THE PRIZE?',
		'section.redeemStaff .copy span'		: 'Visit an appointed Lee shop and show this page to our staff to redeem prize.',
		'section.redeemStaff .staffOnly span'	: 'STAFF ONLY',
		'section.redeemStaff .title2 span'		: 'This step needs to be processed by our staff.',
		'section.redeemStaff .copy2 span'		: 'Please do not swipe the button below or the prize will be forfeited.',
		'section.redeemStaff .swipeSlot span'	: 'SWIPE TO REDEEM',

		// staff more
		'section.redeemStaff .titleS span'			: 'YOU HAVE SUCCESSFULLY REDEEMED THE PRIZE!',
		'section.redeemStaff .prize span.label'		: 'Redeemed prize',
		'section.redeemStaff .prize span.content'	: 'XXXXX',
		'section.redeemStaff .ref span.label'		: 'Reference number',

		// langage page
		'section.language .copy span'	: 'Please choose your language',
		'.languageOptions .option1'		: 'English',
		'.languageOptions .option2'		: '中文',

		// share
		'SHARE title' 			: 'your friends style is',
		'SHARE button' 			: 'creat your own',
		'SHARE fbTitle' 		: 'My Lee canvas inspires me to be a visionary',
		'SHARE fbMessage' 		: 'As Lee Women, we are constantly evolving. What will your canvas be?',

		'SHARE desktop'			: 'Please scan the QR code below and create your own Lee Women canvas now',

		// instagram
		'instagram'				: "TAB AND HOLD TO DOWNLOAD TO YOUR CAMERA ROLL AND SHARE YOUR CANVAS TO INSTAGRAM!",

		// result
		'section.result .you span'	: "Your Lee Women canvas style is",

		// shop
		'section.shops .copy span'	: '<b>Larem Ipsum:</b><ol><li>consectetur adipiscing elit, sed do eiusmod tempor incididunt</li><li>consectetur adipiscing elit, sed do eiusmod tempor incididunt</li><li>consectetur adipiscing elit, sed do eiusmod tempor incididunt</li><li>consectetur adipiscing elit, sed do eiusmod tempor incididunt</li><li>consectetur adipiscing elit, sed do eiusmod tempor incididunt</li><li>Time: <br>- 2015/2/2/ - 2015/2/2</li><li>Dsddsd:<ol><li>consectetur adipiscing elit, sed do eiusmod tempor incididunt</li><li>consectetur adipiscing elit, sed do eiusmod tempor incididunt</li><li>consectetur adipiscing elit, sed do eiusmod tempor incididunt</li></ol></li><li>Lorem:<br>consectetur adipiscing elit, sed do eiusmod tempor incididunt<br>consectetur adipiscing elit, sed do eiusmod tempor incididunt</li><li>consectetur adipiscing elit, sed do eiusmod tempor incididuntconsectetur adipiscing elit, sed do eiusmod tempor incididunt</li><li>参加者须保证上传照片由他/consectetur adipiscing elit, sed do eiusmod tempor incididuntconsectetur adipiscing elit, sed do eiusmod tempor incididunt</li><li>consectetur adipiscing elit, sed do eiusmod tempor incididunt<ol><li>consectetur adipiscing elit, sed do eiusmod tempor incididunt IP consectetur adipiscing elit, sed do eiusmod tempor incididunt</li><li>consectetur adipiscing elit, sed do eiusmod tempor incididunt</li><li>consectetur adipiscing elit, sed do eiusmod tempor incididunt</li></ol>consectetur adipiscing elit, sed do eiusmod tempor incididunt</li><li>consectetur adipiscing elit, sed do eiusmod tempor incididunt consectetur adipiscing elit, sed do eiusmod tempor incididuntconsectetur adipiscing elit, sed do eiusmod tempor incididunt</li><li>consectetur adipiscing elit, sed do eiusmod tempor incididunt</li><li>consectetur adipiscing elit, sed do eiusmod tempor incididuntconsectetur adipiscing elit, sed do eiusmod tempor incididunt</li></ol>',

		'section.shops .link'	: 'http://',

		//fb reminder
		'.fb .reminder':'eng chinreminder'

	};

var sampleResults =
{
  "q1Labels": {
    "feminine": "Feminine",
    "classic": "Causal Chic",
    "boyish": "Boyish",
    "playful": "Playful"
  },
  "q2Labels": {
    "sport": "Take up a new sport",
    "restaurant": "Try out a new resturant",
    "exploreArts": "Experience arts & culture",
    "exploreCity": "Explore the city"
  },
  "q3Labels": {
    "nyc": "New York",
    "london": "London",
    "paris": "Paris",
    "amsterdam": "Amsterdam",
    "berlin": "Berlin",
    "tokyo": "Tokyo"
  },
  "types": {
    "tokyo": {
      "art": "ORIENTAL",
      "style": {
        "feminine": "Your canvas encompasses the elegance and quality of oriental art. You are great at reading into other’s feelings and motivations, and so your friendships grow stronger and deeper. Your strength also lies in your positivity and willingness to try pretty much anything.",
        "classic": "There is an element of mystique in your oriental canvas. You are dedicated and hard-working, but you also have a wild and creative side. You love a place with energy and passion. Your shyness keeps you from the spotlight, but you are the first to lend a helping hand when needed.",
        "boyish": "Your canvas demonstrates the sophistication in oriental art. Your open-mindedness and adaptability towards matters in life leads you to takes things one step at a time. You value harmony and you work hard to ensure that every voice is heard.",
        "playful": "Your canvas strikes a fine balance between the subtlety of oriental art and displays of color. When people first get to know you, they might think you’re a bit reserved. But once they’ve spent some time with you, you’ll be friends for life. Make sure you leave some time for yourself as well, exploring your spiritual side with some peace and quiet beyond the hustle and bustle."
      }
    },
    "paris": {
      "art": "WATERCOLOR",
      "style": {
        "feminine": "Your watercolor canvas is dreamy and pastel. Perhaps you find yourself guided by principles all the time but try letting go occasionally, you never know what hidden gems you might come across.",
        "classic": "Your watercolor canvas is calm yet powerful. You want to live a life of passion and vigor - no one knows how to enjoy a hobby as well as you do. Don’t let your energy go to waste because life is short. Bon voyage!",
        "boyish": "Your watercolor canvas is refreshing yet balanced. Although you prefer a stable and secure lifestyle, you quietly cultivate your own unique and eclectic style. You go wherever life takes you, and the City of Lights is no doubt a good place to start. ",
        "playful": "You are a hopeless romantic and your watercolor canvas explores all the warm undertones of this. Stand under the glittering Eiffel Tower at night or learn the language of love – maybe someday you’ll find someone to say je t’aime to in the City of Love.  You are a hopeless romantic and your watercolor canvas explores all the warm undertones of this. Stand under the glittering Eiffel Tower at night or learn the language of love – maybe someday you’ll find someone to say je t’aime to in the City of Love. "
      }
    },
    "nyc": {
      "art": "POP ART",
      "style": {
        "feminine": "Your charismatic pop art canvas combines your vision, intelligence and determination - you push through to completion no matter the obstacles. You love the fast-paced beat of the concrete jungle and are constantly striving for challenges. There is nothing you can’t do!",
        "classic": "Your pop art canvas is a reflection of your strive for perfection. You are a born leader, and using your ruthless rationality, drive and determination, you can successfully achieve whatever goals you have set for yourself. One day, you will find yourself at the top in the Big Apple.",
        "boyish": "Your canvas is an illuminating modern rendition of pop art. Freedom is important to you and you love a place with varieties. Your independence and creativity will take you far beyond a boring life. Along with the Statue of Liberty, you can never be tied down.",
        "playful": "Your canvas reflects the energy and passion of pop art. You’re an interesting person with a whole range of exciting habits. You pay a great deal of attention to your friends and you’d always end up with deep, meaningful discussions. You will have no problems in making friends and loving life in the city that never sleeps."
      }
    },
    "amsterdam": {
      "art": "OIL PAINTING",
      "style": {
        "feminine": "Your canvas is a colorful oil painting. Your energy brings you into the spotlight with your exciting personality. You crave creativity and freedom as nothing can hold you down. You have an irresistible charm and your warmth, excitement and passions are simply alluring.",
        "classic": "Some may be fooled by the simple façade of your canvas but the oil painting you create goes far beyond. You are fiercely independent and crave creativity and freedom. You believe that there are no irrelevant actions, as every move is a part of something bigger. You live in the moment and your energy and enthusiasm makes you the life of the party. ",
        "boyish": "Your canvas is a refreshing and unique oil painting. You like to go out and experience things. Your imagination can take you places you’d never expect, but your open-mindedness sees all things as a part of a big, mysterious puzzle called life.",
        "playful": "Your oil painting canvas is full of warmth and vigor. Your ability to notice even the smallest of details and strong people skills makes you an ideal companion for a chat over the glistening canals. But whilst listening is your forte, don’t let the overthinking trip you over."
      }
    },
    "london": {
      "art": "STREET ART",
      "style": {
        "feminine": "undefined",
        "classic": "Your canvas is street art that is unique and unconventional. You probably love city life, but without all the hype. You keep your feet on the ground and you make clear, rational decisions. Though structure can be a good thing, don’t just leave everything to the books and take that spontaneous trip you’ve longed for!",
        "boyish": "Your canvas displays all the creativity and color of street art. You are honest and loyal to your friends and you stay by their sides no matter what. Your willingness to take action as a show of support will result in long years of friendships. You are also sharp, and have the ability to come up with a lightning quick wit.",
        "playful": "Your street art canvas is an explosion of color. You’re all about the activity, going out and finding something to do, even when the weather is bad. You view dependence as a weakness, for it is better to be alone than in bad company. After all, the gloomy weather might just be the companion for you! "
      }
    },
    "berlin": {
      "art": "CONTEMPORARY",
      "style": {
        "feminine": "undefined",
        "classic": "Your canvas is a bold rendition of postmodern contemporary art. Your natural confidence, dedication and creative intelligence will open the doors to the increased complexity and freedom you crave. It will also make you capable of doing anything you set your mind to. ",
        "boyish": "Your contemporary canvas represents the freedom and simplicity of the artistic style. You accept unfamiliar territory because of your open-mindedness. You like to live in the middle of a city, where everything is within walking distance. But when you close your door, the whole world goes away and leaves you in peace.",
        "playful": "Your contemporary canvas pays attention to the small details. Your mind constantly gathers information due to your energy on observing the world; therefore you are tremendously insightful and are quick to understand new ideas. You are gifted with leadership skills, but you remain in the dark until you see a read need to take over the lead. "
      }
    }
  }
}

var sampleAssets = {
  "section.intro .intro1 .title": "assets/1441008881654.png",
  "section.intro .intro2 .title": "assets/1441008885295.png",
  "section.intro .intro3 .title": "assets/1441008889576.png",
  "section.redeem .dianpingBlock img": "assets/1441011034732.png",
  "section.redeemDianping .dianpingBlock img": "assets/1441008881654.png",
  "section.draw .dianpingBlock img": "assets/1441008889576.png",

	// new
	// 'section.q1 .content img.question'			: "img/q1_question.png",
	// 'section.q2 .content img.question'			: "img/q2_question.png",
	// 'section.q3 .content img.question'			: "img/q3_question.png",

	// desktop
	'.desktop.copy'								: "img/desktop_copy.png",
	'.desktop.qr'								: "img/desktop_qr.png",
};

var sampleQuotes = {
  "quote_1": "quotes/1441008745923.png",
  "quote_2": "quotes/1441008757877.png",
  "quote_3": "quotes/1441008761707.png",
  "quote_4": "quotes/1441008765947.png",
  "quote_5": "quotes/1441008770044.png",
  "quote_6": "quotes/1441008775117.png",
  "quote_7": "quotes/1441008779900.png",
  "quote_8": "quotes/1441008783805.png",
  "quote_9": "quotes/1441008787980.png",
  "quote_10": "quotes/1441008792054.png",
  "quote_11": "quotes/1441008796149.png",
  "quote_12": "quotes/1441008800399.png",
  "quote_13": "quotes/1441008804627.png",
  "quote_14": "quotes/1441008808404.png",
  "quote_15": "quotes/1441008812856.png",
  "quote_16": "quotes/1441008817815.png",
  "quote_17": "quotes/1441008821818.png",
  "quote_18": "quotes/1441008826730.png",
  "quote_19": "quotes/1441008831386.png",
  "quote_20": "quotes/1441008836175.png",
  "quote_21": "quotes/1441008840766.png",
  "quote_22": "quotes/1441008845905.png",
  "quote_23": "quotes/1441008850643.png",
  "quote_24": "quotes/1441008855370.png"
};

var prize = "on";	// ['on', 'nocode', 'off', 'staff']
