var config		= require('./config.js');
var dan			= require('./dan.js');
var db			= require('./db.js');
var ObjectID = require('mongodb').ObjectID;
var fs = require('fs');
var json2csv 	= require('json2csv');


var api = module.exports = {
	routes: function(app) {
		app.get('/api/test', this.test);
		app.post('/api/onPreview', this.onPreview);
		app.post('/api/getJSON', this.getJSON);
		app.post('/api/login', this.login);
		app.post('/api/imgUpload', this.imgUpload);
		app.post('/api/save', this.save);
		app.post('/api/coupon_upload', this.coupon_upload);

		//report
		app.post("/api/getNumCanvases",		this.getNumCanvases);
		app.post("/api/getNumDianpings",	this.getNumDianpings);
		app.post("/api/getNumDraws",		this.getNumDraws);
		app.get("/api/getAllDraws",			this.getAllDraws);
		app.get("/api/getDraws",			this.getDraws);
	},


	coupon_upload:function(req,res){
		var arr = req.body.dataArr;
		var country = req.body.country;
		var num=0;
		var done;

		db.mongo
		.collection('coupons_'+country)
		.find({},function(err,item){
			if (item) {
				db.mongo
				.collection('coupons_'+country)
				.remove({},function(){
					for (var i = 0; i < arr.length; i++) {
						var data = {
							"code" : arr[i],
							"redeemed" : null
						};
					db.mongo
					.collection('coupons_'+country)
					.insert(data,function(err, items) {
				    if (err) {
							res.send('wrong');
							return;
						}
						if (items) {
							num++;
							if (num==arr.length) {
								console.log('done');
								res.send('done');
							}
						}
					});
					}
				});
			}
			else{
				for (var i = 0; i < arr.length; i++) {
					var data = {
						"code" : arr[i],
						"redeemed" : null
					};
				db.mongo
				.collection('coupons_'+country)
				.insert(data,function(err, items) {
			    if (err) {
						res.send('wrong');
						return;
					}
					if (items) {
						num++;
						if (num==arr.length) {
							console.log('done');
							res.send('done');
						}
					}
				});
				}

			}
		})

	},

	test: function(req, res) {
		res.send('api test');
	},

	login: function(req, res) {
		var country	= req.body.country;
		var pw	= req.body.pw;

		if (country && pw) {
			db.mongo
			.collection('users')
			.findOne({'country':country},function(err, items) {

		    if (err) { /* handle err */ }

		    if (items) {
		        if (items.pw===pw) {
							res.send(items);
		        }
						else{
							res.send('wrong');
						}
		    } else {
		      res.send('wrong');
		    }
			});
		}
	},

	onPreview: function(req, res) {
		var lang	= req.body.lang;
		db.mongo
			.collection('cms')
			.find({'lang':lang})
			.sort({'date': -1})
			.limit(1)
			.each(function(err, items) {
				if (items) {
					var data = {
						'date':items.date,
						'published':items.published,
						'copies':JSON.parse(items.copies),
						'results':JSON.parse(items.results),
						'images':JSON.parse(items.images),
						'quotes':JSON.parse(items.quotes),
						'prize': items.prize,
						'languages':items.languages
					};
					res.send(data);
				}else{
					res.send('no record');
				}
			});
	},

	getJSON: function(req, res) {
		var lang	= req.body.lang;
		var published	= req.body.published;

		if (!published) {
			published="1";
		}

		db.mongo
			.collection('cms')
			.find({'lang':lang,'published':published})
			.sort({'date': -1})
			.limit(1)
			.each(function(err, items) {
				if (items) {
					var data = {
						'copies':JSON.parse(items.copies),
						'results':JSON.parse(items.results),
						'images':JSON.parse(items.images),
						'quotes':JSON.parse(items.quotes),
						'prize': items.prize,
						'languages': items.languages
					};
					res.send(data);
				}else{
					res.send('no record');
				}
			});
	},

	imgUpload: function(req, res) {
		var imageData	= req.body.imageData;
		var fileType	= req.body.fileType;
		var time = new Date().getTime().toString();
		var fn;

		if (fileType==='assets')
		{
			fn = config.ASSETS_PATH + time + '.png';
		}
		else{
			fn = config.QUOTES_PATH + time + '.png';
		}

		if (imageData) {
			dan.writeFile(imageData, 'public/'+fn, function() {
				res.send(fn);
			});
			return;
		}
	},

	save: function(req, res) {

		var lang = req.body.lang;
		var published = req.body.published;
		var copies = req.body.copies;
		var results = req.body.results;
		var images = req.body.images;
		var quotes = req.body.quotes;
		var prize = req.body.prize;
		var languages = req.body.languages;

		var data = {
			'lang':lang,
			'date':new Date(),
			'published':published,
			'copies':copies,
			'results':results,
			'images':images,
			'quotes':quotes,
			'prize':prize,
			'languages':languages
		};

		db.mongo
		.collection('cms')
		.count(function (err, count) {
    if (!err && count === 0) {
				db.mongo
					.collection('cms')
					.insert(data,function(err, item){
						res.send(item);
					});
	    }else{
				if (data.published=="0") {
					db.mongo
						.collection('cms')
						.update({'lang':lang,'published':published},{
						$set: {
							'lang':lang,
							'date':new Date(),
							'copies':copies,
							'results':results,
							'images':images,
							'quotes':quotes,
							'prize':prize
					 	}},
						function(){
							res.send('update');
						});
				}
				else{
					db.mongo
						.collection('cms')
						.insert(data,function(){
							res.send('publish');
						});
				}
			}
		});
	},



	//report
	getNumCanvases: function(req, res) {
		var fromTime	= parseInt(req.body.fromTime),
			toTime 		= parseInt(req.body.toTime),
			gallery		= req.body.gallery,
			countryCode = req.body.countryCode;

		var cond = {
			added : {
				"$gte" : fromTime,
				"$lt" : toTime}
			};

		if (gallery == "true")
			cond.gallery = "true";

		db.mongo
			.collection('posters_'+countryCode)
			.find(cond)
			.count(function(err, total) {
				var data = {total:total};
				res.send(data);
			});
	},

	getNumDianpings: function(req, res) {
		var fromTime	= parseInt(req.body.fromTime),
			toTime 		= parseInt(req.body.toTime),
			countryCode = req.body.countryCode;

		db.mongo
			.collection('coupons_' + countryCode)
			.find({redeemed:{"$gte":new Date(fromTime), "$lt":new Date(toTime)}})
			.count(function(err, total) {
				var data = {total:total};
				res.send(data);
			});
	},

	getNumDraws: function(req, res) {
		var fromTime	= parseInt(req.body.fromTime),
			toTime 		= parseInt(req.body.toTime),
			countryCode = req.body.countryCode;

		db.mongo
			.collection('draws_' + countryCode)
			.find({added:{"$gte":new Date(fromTime), "$lt":new Date(toTime)}})
			.count(function(err, total) {
				var data = {total:total};
				res.send(data);
			});
	},

	getDraws: function(req, res) {

		var fromTime	= parseInt(req.query.fromTime),
			toTime 		= parseInt(req.query.toTime),
			countryCode = req.query.countryCode,
			delta = 8*60*60;

		var ret = {ok:true};

		db.mongo
			.collection('draws_' + countryCode)
			.find({added:{
				"$gte":new Date(fromTime),
				"$lt":new Date(toTime)}
			})
			.sort({added:1})
			.toArray(function(err, items) {
				var ret = [];

				for (var i=0; i<items.length; i++) {
					var item = items[i];
					var time = new Date((item.added.getTime()/1000 + delta) * 1000);

					var obj = {	name 		: item.name,
								number 		: item.number,
								email 		: item.email,
								added 		: time.toISOString().replace("T", " ").split(".")[0]
							};
					ret.push(obj);
				}

				// csv
				var fields = ['name', 'number', 'email', 'added'];
				json2csv({ data: ret, fields: fields }, function(err, csv) {
					var t = new Date(fromTime);
					var fn = "leewomen-" + t.toISOString().split("T")[0] + "-"+countryCode+".csv";
					res.attachment(fn);
					res.setHeader('Content-Type', 'application/octet-stream');
					res.end(csv);
				});
			});
	},

	getAllDraws: function(req, res) {

		var fromTime	= parseInt(req.query.fromTime),
			toTime 		= parseInt(req.query.toTime),
			countryCode = req.query.countryCode,
			delta = 8*60*60;

		var ret = {ok:true};

		db.mongo
			.collection('draws_' + countryCode)
			.find({added:{
				"$gte":new Date(fromTime),
				"$lt":new Date(toTime)}
			})
			.sort({added:1})
			.toArray(function(err, items) {
				var ret = [];

				for (var i=0; i<items.length; i++) {
					var item = items[i];
					var time = new Date((item.added.getTime()/1000 + delta) * 1000);

					var obj = {	name 		: item.name,
								number 		: item.number,
								email 		: item.email,
								added 		: time.toISOString().replace("T", " ").split(".")[0]
							};
					ret.push(obj);
				}

				// csv
				var fields = ['name', 'number', 'email', 'added'];
				json2csv({ data: ret, fields: fields }, function(err, csv) {
					var t = new Date(fromTime);
					var fn = "Full report to date_"+countryCode+".csv";
					res.attachment(fn);
					res.setHeader('Content-Type', 'application/octet-stream');
					res.end(csv);
				});
			});
	},

};
