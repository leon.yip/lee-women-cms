var SETTINGS = {
	// develop
	DEVELOP: {
		PORT 			: 9999,
		BASE_URL	 	: "http://localhost:9999/",
		DB_URI			: "mongodb://localhost:27017/leewomen-apac",
		SERVER_URI	: "http://lang.leewomen.com/",
		ASSETS_PATH	: "assets/",
		QUOTES_PATH	: "quotes/"
	},

	// staging
	STAGING: {
		PORT 			: 9999,
		BASE_URL	 	: "http://women-staging.lee-create.com/",
		DB_URI			: "mongodb://localhost:27017/leewomen-apac",
		ASSETS_PATH	: "public/assets/",
		QUOTES_PATH	: "public/quotes/"
	},

	// production
	PRODUCTION: {
		PORT 			: 9999,
		BASE_URL	 	: "http://women.lee-create.com/",
		DB_URI			: "mongodb://localhost:27017/leewomen-apac",
		SERVER_URI	: "http://lang.leewomen.com/",
		ASSETS_PATH	: "public/assets/",
		QUOTES_PATH	: "public/quotes/"
	}
};

var config = module.exports = {
	init: function() {
		var v = process.env.ENV;

		if (v != "STAGING" && v != "PRODUCTION") {
			v = "DEVELOP";
			console.log("For staging and production: \n");
			console.log("   ENV=STAGING node server.js");
			console.log("   ENV=STAGING forever start server.js\n");
		}

		this.ENV = v;

		for (k in SETTINGS[v]) {
			this[k] = SETTINGS[v][k];
		}
	}
};

config.init();
