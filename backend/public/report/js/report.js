
var Admin = function() {
	console.log('admin');

	this.countryCode = document.getElementById('countrySelector').value;
	console.log('selected country: ' + this.countryCode);

	var delta = 8*60*60,
		day = 24*60*60*1000;

	var now = (new Date()).getTime();
	this.toTime = now - delta;
	this.fromTime = Math.floor(now/day)*day - delta;
	this.earliestTime = (new Date('2015-08-22 0:0:0')).getTime() - delta;

	console.log(new Date(this.fromTime));
	console.log(new Date(this.toTime));
	//
	var allDraws =
		$(document.createElement('div'))
				.addClass('row')
				.html("<a href='/api/getAllDraws?fromTime=" + this.earliestTime + "&toTime=" + this.toTime + "&countryCode=" + this.countryCode +
				"'>Download full report to date</a>")
				.appendTo($('section.data'));

	this.getData();
};

Admin.prototype.getData = function() {
	if (this.fromTime < this.earliestTime)
		return;

	var row = $(document.createElement('div'))
				.addClass('row')
				.addClass('c' + this.fromTime)
				.appendTo($('section.data'));

	// date
	var date = (new Date(this.fromTime)) + "";
	date = date.split(" ");
	$(document.createElement('div'))
		.addClass('date')
		.html(date[1] + " " + date[2])
		.appendTo(row);

	// numPosters
	$(document.createElement('div'))
		.addClass('numCanvasesTotal')
		.appendTo(row);

	// numPosters
	$(document.createElement('div'))
		.addClass('numCanvasesGallery')
		.appendTo(row);

	// dianpings
	$(document.createElement('div'))
		.addClass('numDianpings')
		.appendTo(row);

	// draws
	$(document.createElement('div'))
		.addClass('numDraws')
		.appendTo(row);

	// getDraws
	$(document.createElement('div'))
		.addClass('getDraws')
		.html("<a href='/api/getDraws?fromTime=" + this.fromTime + "&toTime=" + this.toTime + "&countryCode=" + this.countryCode +"'>Download</a>")
		.appendTo(row);

	// get data
	(function(fromTime, toTime, countryCode) {
		$.post(	"/api/getNumCanvases",
				{	fromTime: 	fromTime,
					toTime: 	toTime,
					countryCode: countryCode},
				function(data) {
					$('.c' + fromTime + " .numCanvasesTotal").html(data.total);
				});
	})(this.fromTime, this.toTime, this.countryCode);

	(function(fromTime, toTime, countryCode) {
		$.post(	"/api/getNumCanvases",
				{	fromTime: 	fromTime,
					toTime: 	toTime,
					gallery: "true",
				countryCode: countryCode},
				function(data) {
					$('.c' + fromTime + " .numCanvasesGallery").html(data.total);
				});
	})(this.fromTime, this.toTime, this.countryCode);

	// get draws
	(function(fromTime, toTime, countryCode) {
		$.post(	"/api/getNumDraws",
				{	fromTime: 	fromTime,
					toTime: 	toTime,
				countryCode: countryCode},
				function(data) {
					$('.c' + fromTime + " .numDraws").html(data.total);
				});
	})(this.fromTime, this.toTime, this.countryCode);

	// get dianpings
	(function(fromTime, toTime, countryCode) {
		$.post(	"/api/getNumDianpings",
				{	fromTime: 	fromTime,
					toTime: 	toTime,
				countryCode: countryCode},
				function(data) {
					$('.c' + fromTime + " .numDianpings").html(data.total);
				});
	})(this.fromTime, this.toTime, this.countryCode);

	this.timer = setTimeout(function() {
		this.toTime 	= this.fromTime;
		this.fromTime 	-= 24*60*60*1000;
		this.getData();
	}.bind(this), 1000);
};

Admin.prototype.clearTimer = function(){
	clearTimeout(this.timer);
};

var admin;

$(document).ready(function() {
	document.getElementById('countrySelector').onchange=function(){
		$('.data').html('');
		if (admin){
			admin.clearTimer();
			admin = null;
		}
		admin = new Admin();
	};
});
