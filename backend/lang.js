var express		= require('express');
var config 		= require('./config.js');
var api			= require('./api.js');
var db			= require('./db.js');
var app			= express();


var initApp = function() {
	app.configure(function() {
		app.use(express.json({limit:'10mb'}));
		app.use(express.urlencoded({limit:'10mb'}));
		app.use(function(req, res, next) {
			res.header("Access-Control-Allow-Origin", "*");
			res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
			next();
		});

		app.use(function(req, res, next) {
			console.log('%s %s %s %s', req.ip, req.method, req.url, req.path);
			next();
		});

		app.use('/',					express.static(__dirname + "/public/"));
		api.routes(app);
	});

	app.listen(config.PORT);
	console.log('listening on ' + config.PORT + "...");

};

db.connect(initApp);
