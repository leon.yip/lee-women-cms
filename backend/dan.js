var fs	= require('fs');

var dan = module.exports = {
	writeFile: function(data, fn, cb) {
	    var base64Data = data.replace(/^data:image\/\w+;base64,/, "");
	    var dataBuffer = new Buffer(base64Data, 'base64');
	    fs.writeFile(fn, dataBuffer, function(err) {
	        if(err){
	        	console.log('writeFile(): Failed');
	        	return;
	        }

        	console.log('writeFile(): OK!');
        	cb();
	    });
	}
};
